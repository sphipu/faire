package faire

import (
	"path"

	"github.com/dop251/goja"
)

func (f *F) newPathAPI() goja.Value {
	api := f.rt.NewObject()
	api.Set("base", path.Base)
	api.Set("clean", path.Clean)
	api.Set("dir", path.Dir)
	api.Set("ext", path.Ext)
	api.Set("isAbs", path.IsAbs)
	api.Set("join", path.Join)
	api.Set("match", func(pattern, name string) bool {
		match, err := path.Match(pattern, name)
		if err != nil {
			panic(f.rt.ToValue(pattern + " is an invalid pattern"))
		}
		return match
	})
	api.Set("split", path.Split)

	return api
}
