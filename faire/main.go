package main

//go:generate go-bindata -out assets.go fairefile-template.txt fairefile.d.ts
import (
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"strings"

	"github.com/dop251/goja"
	"gitlab.com/sphipu/faire"
)

var (
	cliInit = flag.Bool("init", false, "Initialize a new faire project")
	// cliInstall = flag.String("install", "", "Install faire to the specified location")
	cliWatch = flag.Bool("watch", false, "Watch all requise(1)d files for changes and rebuild")
	cliForce = flag.Bool("force", false, "obsolete(0) returns true in every case")
)

func runWatchMode(f *faire.F, target string) {
	interrupt := make(chan os.Signal)
	signal.Notify(interrupt, os.Interrupt)
	errs := make(chan error)
	cancel := make(chan struct{})

	go f.Watch(target, errs, cancel)

	for {
		select {
		case err := <-errs:
			if err == nil {
				return
			}
			e := err.(*faire.Error)
			if e.Fatal {
				handleError(err)
			}
			fmt.Fprintln(os.Stderr, e.Message)

		case <-interrupt:
			cancel <- struct{}{}

		}
	}
}

func main() {
	flag.Parse()

	if *cliInit {

		err := os.Mkdir(".faire", os.ModePerm)
		if err != nil && !errors.Is(err, os.ErrExist) {
			handleError(err)
		}
		err = RestoreAsset(".faire", "fairefile.d.ts")
		if err != nil {
			handleError(err)
		}
		if _, err := os.Stat("fairefile.js"); err != nil {
			err = ioutil.WriteFile("fairefile.js", MustAsset("fairefile-template.txt"), 0644)
			if err != nil {
				handleError(err)
			}
		} else {
			fmt.Fprintln(os.Stderr, "fairefile.js exists. Not overwriting")
		}

		return
	}

	faire := faire.NewConfig(faire.Config{Force: *cliForce})
	err := faire.ReadFile("fairefile.js")
	if err != nil {
		handleError(err)
	}

	targets := getCLITargets()
	if len(targets) > 0 {
		if *cliWatch {
			if len(targets) != 1 {
				fmt.Fprintf(os.Stderr, "-watch requires zero to one target not %d\n", len(targets))
				os.Exit(1)
			}
			runWatchMode(faire, targets[0])
			return
		}
		for _, target := range targets {
			err = faire.Faire(target)
			if err != nil {
				handleError(err)
			}
		}
	} else if target, ok := faire.GetDefaultTarget(); ok {
		if *cliForce {
			runWatchMode(faire, target)
			return
		}
		err := faire.Faire(target)
		if err != nil {
			handleError(err)
		}
	} else {
		fmt.Fprintln(os.Stderr, "No rule found")
		os.Exit(1)
	}
}

func handleError(err error) {
	var gojaExc *goja.Exception
	var faireErr *faire.Error
	if errors.As(err, &gojaExc) {
		fmt.Fprintln(os.Stderr, gojaExc.String())
	} else if errors.As(err, &faireErr) {
		fmt.Fprintln(os.Stderr, faireErr.Message)
	} else {
		fmt.Fprintln(os.Stderr, err)
	}
	os.Exit(1)
}

func getCLITargets() (targets []string) {
	for i := 1; i < len(os.Args); i++ {
		if !strings.HasPrefix(os.Args[i], "-") {
			targets = append(targets, os.Args[i])
		}

	}
	return
}
