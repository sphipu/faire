package faire

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"runtime"

	"github.com/dop251/goja"
	"github.com/fsnotify/fsnotify"
)

// Config contains all options faire may take
type Config struct {
	Force bool
}

// F is a Faire context
type F struct {
	cfg                 Config
	rt                  *goja.Runtime
	rules               []rule
	target              string
	prerequisites       []string
	allPrerequisites    map[string]struct{} // store all prerequisites to watch for changes
	phonyTargets        map[string]struct{}
	errProto            *goja.Object
	getPrerequisiteMode bool // allow obsolete(1) to return allways false to just get dependencies
}

// Error is returned if something went wrong
type Error struct {
	Err     error
	Message string
	Fatal   bool
}

func (e *Error) Error() string {
	return "faire: " + e.Message
}

// Unwrap implements errors.Unwrapper for Error
func (e *Error) Unwrap() error {
	return e.Err
}

type rule interface {
	Match(target string) []string
	Callable() goja.Callable
	String() string
}

// New creates a new Faire context
func New() *F {

	faire := &F{
		phonyTargets:     make(map[string]struct{}),
		allPrerequisites: make(map[string]struct{}),
	}
	faire.initRuntime()

	return faire
}

// NewConfig creates a new Faire context using the provided config
func NewConfig(cfg Config) *F {
	faire := &F{
		phonyTargets:     make(map[string]struct{}),
		allPrerequisites: make(map[string]struct{}),
		cfg:              cfg,
	}
	faire.initRuntime()

	return faire
}

// ReadFile reads a Fairefile
func (f *F) ReadFile(fileName string) error {
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	_, err = f.rt.RunScript(fileName, string(content))
	return err
}

func (f *F) initRuntime() {
	f.rt = goja.New()
	f.rt.Set("faire", f.jsFaire)
	f.rt.Set("requise", f.jsRequise)
	f.rt.Set("log", f.jsLog)
	f.rt.Set("obsolete", f.jsObsolete)
	f.rt.Set("exec", f.jsExec)
	f.rt.Set("phony", f.jsPhony)
	f.rt.Set("file", f.newFileAPI())
	f.rt.Set("path", f.newPathAPI())
	f.rt.Set("dir", f.newDirAPI())
	f.rt.Set("env", f.newEnvAPI())
	f.rt.Set("operatingSystem", runtime.GOOS)
	f.rt.Set("extendedErrors", false)

	workDir, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	f.rt.Set("workDir", workDir)

	f.errProto = f.rt.NewObject()
	f.errProto.Set("toString", f.jsErrToString)
}

func (f *F) jsErrToString(call goja.FunctionCall) goja.Value {
	var message string
	var err error

	this := call.This.ToObject(f.rt)

	if err := f.rt.ExportTo(this.Get("message"), &message); err != nil {
		panic(err)
	}

	if err := f.rt.ExportTo(this.Get("err"), &err); err != nil {
		panic(err)
	}

	if f.rt.Get("extendedErrors").ToBoolean() {
		return f.rt.ToValue(fmt.Sprintf("FaireError: %s: %s", message, err))
	}
	return f.rt.ToValue(fmt.Sprintf("FaireError: %s", message))
}

type stringRule struct {
	Name     string
	callable goja.Callable
}

func (r *stringRule) Match(target string) []string {
	if r.Name == target {
		return []string{target}
	}
	return nil
}

func (r *stringRule) String() string {
	return r.Name
}

func (r *stringRule) Callable() goja.Callable {
	return r.callable
}

type regexpRule struct {
	RegExp   *goja.Object
	callable goja.Callable
	rt       *goja.Runtime
}

func (r *regexpRule) Match(target string) []string {
	exec, _ := goja.AssertFunction(r.RegExp.Get("exec"))
	jsResult, _ := exec(r.RegExp, r.rt.ToValue(target))
	result := []string{}
	r.rt.ExportTo(jsResult, &result)
	return result
}

func (r *regexpRule) String() string {
	return r.RegExp.String()
}

func (r *regexpRule) Callable() goja.Callable {
	return r.callable
}

// GetDefaultTarget returns the first string target
func (f *F) GetDefaultTarget() (string, bool) {
	for _, rule := range f.rules {
		if rule, ok := rule.(*stringRule); ok {
			return rule.Name, true
		}
	}
	return "", false
}

func (f *F) jsFaire(call goja.FunctionCall) goja.Value {
	rule := call.Argument(0)
	callable, callableOK := goja.AssertFunction(call.Argument(1))
	if !rule.ToBoolean() || !callableOK {
		panic(f.rt.ToValue("Invalid rule " + rule.String()))
	}
	targetClassName := rule.ToObject(f.rt).ClassName()

	if targetClassName == "String" {
		f.rules = append(f.rules, &stringRule{Name: rule.Export().(string), callable: callable})
	} else if targetClassName == "RegExp" {
		f.rules = append(f.rules, &regexpRule{RegExp: rule.ToObject(f.rt), callable: callable, rt: f.rt})
	} else {
		panic(f.rt.ToValue("Invalid rule " + rule.String()))
	}

	return goja.Undefined()
}

func (f *F) jsLog(call goja.FunctionCall) goja.Value {
	for _, value := range call.Arguments {
		fmt.Print(value.String())
	}
	fmt.Println()
	return goja.Undefined()
}

func (f *F) jsRequise(target string) {
	if !f.isPhonyTarget(target) {
		f.prerequisites = append(f.prerequisites, target)
		f.allPrerequisites[target] = struct{}{}
	}

	oldTarget := f.target
	oldPrerequisites := f.prerequisites
	defer func() {
		f.target = oldTarget
		f.prerequisites = oldPrerequisites
	}()
	f.target = target
	f.prerequisites = nil
	err := f.faire()
	if err != nil {
		if _, ok := err.(*goja.Exception); ok {
			panic(err)
		}
		panic(f.rt.ToValue(err.Error()))

	}
}

func (f *F) isPhonyTarget(target string) (isPhonyTarget bool) {
	_, isPhonyTarget = f.phonyTargets[target]
	return
}

func (f *F) jsObsolete() bool {
	if f.getPrerequisiteMode {
		return false
	}

	if f.cfg.Force {
		return true
	}

	if f.isPhonyTarget(f.target) {
		return true
	}

	targetStats, err := os.Stat(f.absPath(f.target))
	if err != nil {
		// The target does not exist
		return true
	}
	targetModTime := targetStats.ModTime()
	faireFileStats, err := os.Stat("fairefile.js")

	if err != nil || faireFileStats.ModTime().After(targetModTime) {
		// The faire file was changed (or some wired error happend)
		return true
	}

	for _, prerequisite := range f.prerequisites {
		preStats, err := os.Stat(f.absPath(prerequisite))
		if err != nil {
			// A prerequisite was not created
			return true
		}
		if preStats.ModTime().After(targetModTime) {
			// A dependency is newer than the target
			return true
		}
	}

	return false
}

func (f *F) jsExec(program string, args []string, config *goja.Object) *goja.Object {
	if config == nil {
		config = f.rt.NewObject()
	}

	command := exec.Command(program, args...)
	command.Dir = f.workDir()

	var stdoutBuf bytes.Buffer
	var stderrBuf bytes.Buffer

	if pipeStdout := config.Get("pipeStdout"); pipeStdout != nil && pipeStdout.ToBoolean() {
		command.Stdout = &stdoutBuf
	} else {
		command.Stdout = os.Stdout
	}

	if pipeStderr := config.Get("pipeStderr"); pipeStderr != nil && pipeStderr.ToBoolean() {
		command.Stderr = &stderrBuf
	} else {
		command.Stderr = os.Stderr
	}

	if stdin := config.Get("stdin"); stdin != nil {
		var value string
		if err := f.rt.ExportTo(stdin, &value); err != nil {
			panic(f.rt.NewTypeError(fmt.Sprintf("Can not convert %s to string", value)))
		}
		command.Stdin = bytes.NewReader([]byte(value))
	}

	err := command.Run()

	if err != nil {
		var message string
		var exitErr *exec.ExitError

		if errors.As(err, &exitErr) {
			message = fmt.Sprintf("%s exited with code %d", program, exitErr.ExitCode())
		} else if errors.Is(err, exec.ErrNotFound) {
			message = fmt.Sprintf("%s was not found", program)
		} else {
			message = fmt.Sprintf("Call to %s failed", program)
		}

		f.throwErr(message, err)
	}

	res := f.rt.NewObject()
	res.Set("stdout", stdoutBuf.String())
	res.Set("stderr", stderrBuf.String())
	return res
}

// Faire calls the first rule which matches target
func (f *F) Faire(target string) error {
	f.target = target
	return f.faire()
}

func (f *F) faire() error {
	for _, rule := range f.rules {
		if match := rule.Match(f.target); len(match) > 0 {
			_, err := rule.Callable()(goja.Undefined(), f.rt.ToValue(f.target), f.rt.ToValue(match))
			if err != nil {
				return &Error{err, "Can not faire rule" + rule.String(), false}
			}
			return nil
		}
	}

	// If there is no rule to create a file, and the target is not
	// a phony target, check if the target exists before failing
	if !f.isPhonyTarget(f.target) {
		_, err := os.Stat(f.target)
		if err == nil {
			return nil
		}
	}
	return &Error{nil, "No rule to make target " + f.target, true}
}

func (f *F) jsPhony(targets ...string) {
	for _, target := range targets {
		f.phonyTargets[target] = struct{}{}
	}
}

func (f *F) throwErr(message string, err error) {
	o := f.rt.CreateObject(f.errProto)
	o.Set("message", message)
	o.Set("err", err)

	panic(o)
}

func (f *F) workDir() string {
	return f.rt.Get("workDir").Export().(string)
}

func (f *F) absPath(p string) string {
	if !path.IsAbs(p) {
		return path.Join(f.workDir(), p)
	}
	return p
}

// Watch faires a target and watches ALL dependencies for changes
func (f *F) Watch(target string, errs chan<- error, cancel <-chan struct{}) {
	defer close(errs)

	// Get the dependencies of target
	f.getPrerequisiteMode = true
	err := f.Faire(target)
	f.getPrerequisiteMode = false
	if err != nil {
		errs <- &Error{err, "Can not get prerequisites of " + target, true}
		return
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		errs <- &Error{err, "Can not create fsnotify watcher", true}
		return
	}
	defer watcher.Close()

	fmt.Println("Faire " + target)
	err = f.Faire(target)
	if err != nil {
		errs <- &Error{err, "Can not faire target " + target, false}
	}

	for fileName := range f.allPrerequisites {
		err := watcher.Add(fileName)
		if err != nil {
			errs <- &Error{err, "Can not watch file " + fileName, false}
		}
	}

	dropFSEvents := false

	for {
		select {
		case <-cancel:
			return

		case err := <-watcher.Errors:
			errs <- &Error{err, "Error while watching files", false}
			return

		case evt := <-watcher.Events:
			if dropFSEvents {
				continue
			}
			if evt.Op == fsnotify.Write {
				dropFSEvents = true
				go func() {
					defer func() { dropFSEvents = false }()
					fmt.Println("File " + evt.Name + " changed. Faire " + target)
					err := f.Faire(target)
					if err != nil {
						errs <- &Error{err, "Can not faire target " + target, false}
					}
				}()
			}
		}
	}
}
