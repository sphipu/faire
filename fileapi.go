package faire

import (
	"io"
	"io/ioutil"
	"os"

	"github.com/dop251/goja"
)

func (f *F) newFileAPI() goja.Value {
	api := f.rt.NewObject()
	api.Set("read", f.jsFileRead)
	api.Set("write", f.jsFileWrite)
	api.Set("exists", f.jsFileExists)
	api.Set("remove", f.jsFileRemove)
	api.Set("move", f.jsFileMove)

	return api
}

func (f *F) jsFileRead(fileName string) string {
	data, err := ioutil.ReadFile(f.absPath(fileName))
	if err != nil {
		f.throwErr("Can not read file "+fileName, err)
	}
	return string(data)
}

func (f *F) jsFileWrite(fileName string, content string) {
	err := ioutil.WriteFile(f.absPath(fileName), []byte(content), 0644)
	if err != nil {
		f.throwErr("Can not write file "+fileName, err)
	}
}

func (f *F) jsFileExists(fileName string) bool {
	_, err := os.Stat(f.absPath(fileName))
	return err == nil
}

func (f *F) jsFileRemove(fileName string) {
	err := os.Remove(f.absPath(fileName))
	if err != nil {
		f.throwErr("Can not remove file "+fileName, err)
	}
}

func (f *F) jsFileMove(srcName, dstName string) {
	err := os.Rename(srcName, dstName)
	if err != nil {
		f.throwErr("Can not move file"+srcName+" to "+dstName, err)
	}
}

func (f *F) jsFileCopy(srcName, dstName string) {
	src, err := os.Open(srcName)
	if err != nil {
		f.throwErr("Can not open source file "+srcName, err)
	}
	defer src.Close()

	dst, err := os.Create(dstName)
	if err != nil {
		f.throwErr("Can not open destination file "+dstName, err)
	}
	defer dst.Close()

	_, err = io.Copy(dst, src)
	if err != nil {
		f.throwErr("Can not copy from "+srcName+" to "+dstName, err)
	}
}
