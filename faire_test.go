package faire

import (
	"os"
	"strings"
	"testing"
)

func TestStringRule(t *testing.T) {
	faire := New()
	_, err := faire.rt.RunString(`faire("Test", function(){})`)
	if err != nil {
		t.Error(err)
		return
	}

	if len(faire.rules[0].Match("test")) > 0 {
		t.Error("test did match 'Test'")
	}

	if len(faire.rules[0].Match("Test")) < 1 {
		t.Error("test did not match 'Test'")
	}
}

func TestRegexRule(t *testing.T) {
	faire := New()
	_, err := faire.rt.RunString(`faire(/Test/, function(){})`)
	if err != nil {
		t.Error(err)
		return
	}

	if len(faire.rules[0].Match("this is a test")) > 0 {
		t.Error("this is a test did match /Test/")
	}

	if len(faire.rules[0].Match("this is a Test")) < 1 {
		t.Error("this is a Test did not match /Test/")
	}
}

func TestGetDefaultTarget(t *testing.T) {
	faire := New()
	faire.rt.RunString(`
		faire(/Test/, function(){})
		faire("all", function(){})
		faire("clean", function(){})
	`)

	if target, ok := faire.GetDefaultTarget(); !ok || target != "all" {
		t.Errorf("GetDefaultTarget did return %s", target)
	}

	faire2 := New()
	if _, ok := faire2.GetDefaultTarget(); ok {
		t.Error("GetDefaultTarget did return true")
	}
}

func TestJSFaire(t *testing.T) {
	faire := New()
	faire.rt.RunString(`faire("all", function(){})`)
	faire.rt.RunString(`faire(/Test/, function(){})`)

	if _, ok := faire.rules[0].(*stringRule); !ok {
		t.Error("faire(2) did not create string rule all")
	}

	if _, ok := faire.rules[1].(*regexpRule); !ok {
		t.Error("faire(2) did not create regex rule /Test/")
	}
}

func TestJSRequise(t *testing.T) {
	faire := New()
	faire.rt.RunString(`var isOk = false; faire("test/testfile.js", function() { isOk = true })`)
	faire.rt.RunString(`requise("test/testfile.js")`)

	if !faire.rt.Get("isOk").ToBoolean() {
		t.Error("requise did not work as expected")
	}
}

func TestPhonyTarget(t *testing.T) {
	faire := New()
	faire.rt.RunString(`phony("all")`)

	if faire.isPhonyTarget("clean") {
		t.Error("clean is not a phony target")
	}

	if !faire.isPhonyTarget("all") {
		t.Error("all is a phony target")
	}
}

func TestJSObsolete(t *testing.T) {
	faire := New()
	faire.rt.RunString(`var isOk = false; phony("all"); faire("all", function() { isOk = obsolete() })`)

	err := faire.Faire("all")
	if err != nil {
		t.Error(err)
		return
	}

	if !faire.rt.Get("isOk").ToBoolean() {
		t.Error("obsolete(0) did return false")
	}
}

func TestJSExec(t *testing.T) {
	faire := New()
	result, err := faire.rt.RunString(`exec("hostname", [], {pipeStdout: true})`)
	if err != nil {
		t.Error(err)
		return
	}

	hostname, err := os.Hostname()
	if err != nil {
		t.Error(err)
		return
	}

	if hn := strings.TrimSpace(result.ToObject(faire.rt).Get("stdout").Export().(string)); hn != hostname {
		t.Errorf("Hostname %s returned from exec is not %s", hn, hostname)
	}
}
